import sys
import numpy

import theano
import theano.tensor as T

COLOR_BOUNDARY = 256


if len(sys.argv) == 2 and sys.argv[1] == '--debug':
    print('*** DEBUG MODE IS ON ***')
    DEBUG = True
else:
    DEBUG = False


def load_kaggle_data(path='./data/train.csv'):
    with open(path) as input_stream:
        train_data = input_stream.read()

    samples = [map(lambda(x): float(x) / COLOR_BOUNDARY, line.split(',')[1:])
               for line in train_data.split('\n')[1:-1]]
    labels = [float(line.split(',')[0])
              for line in train_data.split('\n')[1:-1]]

    train_samples = numpy.array(samples[:-10000], dtype = theano.config.floatX)
    train_labels = numpy.array(labels[:-10000], dtype = theano.config.floatX)
    train_set = (train_samples, train_labels)

    valid_samples = numpy.array(samples[-10000:-5000],
                                dtype = theano.config.floatX)
    valid_labels = numpy.array(labels[-10000:-5000],
                               dtype = theano.config.floatX)
    valid_set = (valid_samples, valid_labels)

    test_samples = numpy.array(samples[-5000:], dtype = theano.config.floatX)
    test_labels = numpy.array(labels[-5000:], dtype = theano.config.floatX)
    test_set = (test_samples, test_labels)

    if DEBUG:
        print(train_samples.shape)
        print(valid_samples.shape)
        print(test_samples.shape)
        print(train_samples.dtype)

    def shared_dataset(data_xy, borrow=True):
        """ Function that loads the dataset into shared variables

        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        """
        data_x, data_y = data_xy
        shared_x = theano.shared(numpy.asarray(data_x,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        shared_y = theano.shared(numpy.asarray(data_y,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        # When storing data on the GPU it has to be stored as floats
        # therefore we will store the labels as ``floatX`` as well
        # (``shared_y`` does exactly that). But during our computations
        # we need them as ints (we use labels as index, and if they are
        # floats it doesn't make sense) therefore instead of returning
        # ``shared_y`` we will have to cast it to int. This little hack
        # lets ous get around this issue
        return shared_x, shared_y # T.cast(shared_y, 'int32')

    test_set_x, test_set_y = shared_dataset(test_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    train_set_x, train_set_y = shared_dataset(train_set)

    rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]
    return rval

if __name__ == '__main__':
    load_kaggle_data()
