# network classification of simple cases

import sys
import random
import numpy
import theano
import theano.tensor as T
import matplotlib.pyplot as plotter
import matplotlib.cm as colormap

from load_kaggle_data import load_kaggle_data

sys.path.append('/home/agrin/Install/grinlib')
from layer import HiddenLayer
from regression import LogisticRegression
from augmentation import augmentate
from augmentation import get_deformed_data

# theano.config.compute_test_value = 'warn'
# 0.017

from constants import RANDOM_SEED
from constants import BATCH_SIZE
from constants import LEARNING_RATE
from constants import LEARNING_RATE_DECAY
from constants import NUMBER_OF_EPOCHS

from constants import INPUT_LAYER_SIZE
from constants import FIRST_HIDDEN_LAYER_SIZE
from constants import SECOND_HIDDEN_LAYER_SIZE
from constants import OUTPUT_LAYER_SIZE

FINAL = False

from constants import COLOR_BOUNDARY

if __name__ == '__main__':
  # Lading data
  datasets = load_kaggle_data()
  shared_train_samples, shared_train_labels = datasets[0]
  shared_validation_samples, shared_validation_labels = datasets[1]
  shared_test_samples, shared_test_labels = datasets[2]

  # for i in xrange(10):
  #   shifted_train_samples = theano.shared(get_deformed_data(shared_train_samples.get_value(),
  #                                                           0, 0, 7, 1.2, 0.8))
  #   plotter.imshow(shared_train_samples.get_value()[i].reshape((28, 28)), cmap=colormap.Greys_r)
  #   plotter.show()
  #   plotter.imshow(shifted_train_samples.get_value()[i].reshape((28, 28)), cmap=colormap.Greys_r)
  #   plotter.show()
  # sys.exit()

  # print(shared_train_samples.get_value().shape)
  # print(shared_train_labels.get_value().shape)
  if FINAL:
    final_train_samples = theano.shared(numpy.concatenate((shared_train_samples.get_value(),
                                                           shared_validation_samples.get_value(),
                                                           shared_test_samples.get_value())))
    final_train_labels = theano.shared(numpy.concatenate((shared_train_labels.get_value(),
                                                          shared_validation_labels.get_value(),
                                                          shared_test_labels.get_value())))

  random_number_generator = numpy.random.RandomState(RANDOM_SEED)
  minibatch_index_var = T.lscalar()
  network_input_var = T.matrix('network_input_var')
  # shared_samples = T.matrix('shared_samples')
  # network_input_var.tag.test_value = numpy.zeros((90, 1),
  #                                            dtype=theano.config.floatX)
  network_output_var = T.ivector('network_output_var')
  # shared_labels = T.ivector('shared_labels')
  # network_output_var.tag.test_value = numpy.zeros((90,),
  #                                            dtype=theano.config.floatX)

  # print(shared_validation_samples.get_value().shape)
  # print(shared_validation_labels.get_value().shape)

  first_layer = HiddenLayer(random_number_generator,
                            symbolic_input=network_input_var,
                            number_of_inputs=INPUT_LAYER_SIZE,
                            number_of_outputs=FIRST_HIDDEN_LAYER_SIZE)

  second_layer = HiddenLayer(random_number_generator,
                             symbolic_input=first_layer.output,
                             number_of_inputs=FIRST_HIDDEN_LAYER_SIZE,
                             number_of_outputs=SECOND_HIDDEN_LAYER_SIZE)

  output_layer = LogisticRegression(symbolic_input=second_layer.output,
                                    number_of_inputs=SECOND_HIDDEN_LAYER_SIZE,
                                    number_of_outputs=OUTPUT_LAYER_SIZE)

  # first_layer.load('first_layer.bin')
  # second_layer.load('second_layer.bin')
  # output_layer.load('output_layer.bin')
  # sys.exit()

  cost = output_layer.negative_log_likelihood(network_output_var)

  validation_model = theano.function(
     [minibatch_index_var],
     output_layer.errors(network_output_var),
     givens={
       network_input_var: shared_validation_samples[
                                minibatch_index_var * BATCH_SIZE:
                                (minibatch_index_var + 1) * BATCH_SIZE],
       network_output_var: T.cast(shared_validation_labels[
                                minibatch_index_var * BATCH_SIZE:
                                (minibatch_index_var + 1) * BATCH_SIZE], 'int32')})

  # test_model = theano.function(
  #    [network_input_var, network_output_var],
  #    output_layer.errors(network_output_var))
                                 
                                   
  parameters = output_layer.parameters + second_layer.parameters + first_layer.parameters
  gradients = T.grad(cost, parameters)

  print('Training started')
  improvement_threshold = 0.995
  train_batches_number = (
             shared_train_samples.get_value(borrow=True).shape[0] / BATCH_SIZE)
  validation_batches_number = (
        shared_validation_samples.get_value(borrow=True).shape[0] / BATCH_SIZE)
  validation_frequency = train_batches_number
  best_parameters = None
  
  epoch = 0
  done_looping = False
  current_learning_rate = LEARNING_RATE

  while (epoch < NUMBER_OF_EPOCHS) and (not done_looping):
    print('Epoch: {}'.format(epoch))
    if epoch % 100 == 0:
      print('Dumping network state')
      first_layer.dump('first_layer.bin')
      second_layer.dump('second_layer.bin')
      output_layer.dump('output_layer.bin')
      
    print('learning rate: {}'.format(current_learning_rate))
    vertical_shift = random.randint(-2, 2)
    horizontal_shift = random.randint(-2, 2)
    rotation_angle = random.randint(-7, 7)
    vertical_stretch = random.uniform(0.8, 1.2)
    horizontal_stretch = random.uniform(0.8, 1.2)
    print('vertical shift: {}, horizontal shift: {}, rotation angle: {}'
                                         .format(vertical_shift, horizontal_shift, rotation_angle))
    print('vertical stretch: {}, horizontal stretch: {}'
                                         .format(vertical_stretch, horizontal_stretch))

    if FINAL:
      shifted_train_samples = theano.shared(get_deformed_data(
                                                  final_train_samples.get_value(),
                                                  vertical_shift, horizontal_shift,
                                                  rotation_angle,
                                                  vertical_stretch, horizontal_stretch))
    else:
      shifted_train_samples = theano.shared(get_deformed_data(
                                                  shared_train_samples.get_value(),
                                                  vertical_shift, horizontal_shift,
                                                  rotation_angle,
                                                  vertical_stretch, horizontal_stretch))

    updates = []
    for parameter, gradient in zip(parameters, gradients):
      updates.append((parameter, parameter - current_learning_rate * gradient))
    current_learning_rate *= LEARNING_RATE_DECAY

    train_model2 = theano.function([minibatch_index_var], cost, updates=updates,
           givens={network_input_var: shifted_train_samples[
                                  minibatch_index_var * BATCH_SIZE:
                                  (minibatch_index_var + 1) * BATCH_SIZE],
         network_output_var: T.cast(shared_train_labels[
                                  minibatch_index_var * BATCH_SIZE:
                                  (minibatch_index_var + 1) * BATCH_SIZE], 'int32')})

    epoch += 1
    for minibatch_index in xrange(train_batches_number):
      iterator = (epoch - 1) * train_batches_number + minibatch_index
      # current_cost = train_model(minibatch_index)
      current_cost = train_model2(minibatch_index)
      if not FINAL:
        if (iterator + 1) % validation_frequency == 0:
          validation_losses = [validation_model(i) for i
                                            in xrange(validation_batches_number)]
          current_validation_loss = numpy.mean(validation_losses)
          print('validation error: {}'.format(current_validation_loss))
    print('')


  # Forming prediction output
  if FINAL:
    print('Loading kaggle test set')
    with open('./data/test.csv') as input_stream:
      test_data = input_stream.read()

    kaggle_test_samples = [map(lambda(x) : float(x) / COLOR_BOUNDARY,
                               line.split(','))
                           for line in test_data.split('\n')[1:-1]]

    shared_kaggle_test = theano.shared(numpy.array(kaggle_test_samples,
                                       dtype=theano.config.floatX))

    prediction = output_layer.y_prediction
    prediction_model = theano.function([minibatch_index_var], prediction,
          givens={
            network_input_var: shared_kaggle_test[minibatch_index_var * BATCH_SIZE:
                                              (minibatch_index_var + 1) * BATCH_SIZE]})

    
    print('Prediction started')
    result = []
    for i in xrange(shared_kaggle_test.get_value().shape[0] / BATCH_SIZE):
      new_prediction = prediction_model(i)
      result.extend(list(new_prediction))

    with open('./data/out.csv', 'w') as output_stream:
      output_stream.write('ImageId,Label\n')

      counter = 0
      for value in result:
        counter += 1
        output_stream.write(str(counter) + ',' + str(value) + '\n')

    print('Prediction completed')

