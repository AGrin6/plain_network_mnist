RANDOM_SEED = 42
BATCH_SIZE = 10
LEARNING_RATE = 0.01
LEARNING_RATE_DECAY = 0.996
NUMBER_OF_EPOCHS = 1000

INPUT_LAYER_SIZE = 28 * 28
FIRST_HIDDEN_LAYER_SIZE = 2 * 500
SECOND_HIDDEN_LAYER_SIZE = 2 * 250
OUTPUT_LAYER_SIZE = 10

COLOR_BOUNDARY = 256
