My solution of kaggle MNIST challenge. Based on theano tutorial. 

Several steps towards http://arxiv.org/pdf/1003.0358.pdf. Simple network + unlimited data augmentation + decreasing learning rate.