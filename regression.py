import cPickle
import numpy
import theano
import theano.tensor as T

class LogisticRegression(object):
  """Multi-class Logistic Regression Class

  The logistic regression is fully described by a weight matrix :math:`W`
  and bias vector :math:`b`. Classification is done by projecting data
  points onto a set of hyperplanes, the distance to which is used to
  determine a class membership probability.
  """

  def __init__(self, symbolic_input, number_of_inputs, number_of_outputs):
      """ Initialize the parameters of the logistic regression

      :type symbolic_input: theano.tensor.TensorType
      :param symbolic_input: symbolic variable that describes the input of the
                    architecture (one minibatch)

      :type number_of_inputs: int
      :param number_of_inputs: number of input units, the dimension of the space in
                   which the datapoints lie

      :type number_of_outputs: int
      :param number_of_outputs: number of output units, the dimension of the space in
                    which the labels lie

      """

      # initialize with 0 the weights W as a matrix of shape (number_of_inputs, number_of_outputs)
      self.weights = theano.shared(value=numpy.zeros((number_of_inputs, number_of_outputs),
                                               dtype=theano.config.floatX),
                              name='weights', borrow=True)
      # initialize the baises b as a vector of number_of_outputs 0s
      self.biases = theano.shared(value=numpy.zeros((number_of_outputs,),
                                               dtype=theano.config.floatX),
                             name='biases', borrow=True)

      # compute vector of class-membership probabilities in symbolic form
      self.p_y_given_x = T.nnet.softmax(T.dot(symbolic_input, self.weights) + self.biases)

      # compute prediction as class whose probability is maximal in
      # symbolic form
      self.y_prediction = T.argmax(self.p_y_given_x, axis=1)

      # parameters of the model
      self.parameters = [self.weights, self.biases]

  def negative_log_likelihood(self, y):
    """Return the mean of the negative log-likelihood of the prediction
    of this model under a given target distribution.

    .. math::

        \frac{1}{|\mathcal{D}|} \mathcal{L} (\theta=\{W,b\}, \mathcal{D}) =
        \frac{1}{|\mathcal{D}|} \sum_{i=0}^{|\mathcal{D}|} \log(P(Y=y^{(i)}|x^{(i)}, W,b)) \\
            \ell (\theta=\{W,b\}, \mathcal{D})

    :type y: theano.tensor.TensorType
    :param y: corresponds to a vector that gives for each example the
              correct label

    Note: we use the mean instead of the sum so that
          the learning rate is less dependent on the batch size
    """
    # y.shape[0] is (symbolically) the number of rows in y, i.e.,
    # number of examples (call it n) in the minibatch
    # T.arange(y.shape[0]) is a symbolic vector which will contain
    # [0,1,2,... n-1] T.log(self.p_y_given_x) is a matrix of
    # Log-Probabilities (call it LP) with one row per example and
    # one column per class LP[T.arange(y.shape[0]),y] is a vector
    # v containing [LP[0,y[0]], LP[1,y[1]], LP[2,y[2]], ...,
    # LP[n-1,y[n-1]]] and T.mean(LP[T.arange(y.shape[0]),y]) is
    # the mean (across minibatch examples) of the elements in v,
    # i.e., the mean log-likelihood across the minibatch.
    return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])

  # def get_prediction(self, ...)

  def errors(self, y):
      """Return a float representing the number of errors in the minibatch
      over the total number of examples of the minibatch ; zero one
      loss over the size of the minibatch

      :type y: theano.tensor.TensorType
      :param y: corresponds to a vector that gives for each example the
                correct label
      """

      # check if y has same dimension of y_prediction
      if y.ndim != self.y_prediction.ndim:
          raise TypeError('y should have the same shape as self.y_prediction',
              ('y', target.type, 'y_prediction', self.y_prediction.type))
      # check if y is of the correct datatype
      if y.dtype.startswith('int'):
          # the T.neq operator returns a vector of 0s and 1s, where 1
          # represents a mistake in prediction
          return T.mean(T.neq(self.y_prediction, y))
      else:
          raise NotImplementedError()

  def dump(self, file_name):
    with open(file_name, 'wb') as output_stream:
      numpy.save(output_stream, self.weights.get_value())
      numpy.save(output_stream, self.biases.get_value())

  def load(self, file_name):
    with open(file_name, 'rb') as input_stream:
      self.weights = theano.shared(numpy.load(input_stream))
      self.biases = theano.shared(numpy.load(input_stream))
      # self.number_of_inputs = ...
      # self.number_of_outputs = ...
      print(self.weights.get_value().shape)
      print(self.biases.get_value().shape)


