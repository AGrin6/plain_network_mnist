import copy
import sys
import math
import theano
import numpy
import matplotlib.pyplot as plotter
import matplotlib.cm as colormap
from PIL import Image

COLOR_BOUNDARY = 256

def get_deformed_sample(np_image, vertical_shift=0, horizontal_shift=0, rotation_angle=0,
                        vertical_stretch=1., horizontal_stretch=1.):
  """
  Generates deformed sample
  :type np_image numpy.ndarray
  :param np_image 28x28 numpy ndarray
  """
  assert(np_image.shape == (28, 28))
  result = copy.deepcopy(np_image)

  image = Image.fromarray(numpy.uint8(np_image * COLOR_BOUNDARY))

  # image = image.rotate(rotation_angle)
  # image = image.transform(image.size, Image.AFFINE,
  #                         (vertical_stretch, 0, 0, 0, horizontal_stretch, 0))

  old_center_x = 14.
  old_center_y = 14.
  new_center_x = 14.
  new_center_y = 14.
  radians_angle = -rotation_angle / 180.0 * math.pi

  a = math.cos(radians_angle) / horizontal_stretch
  b = math.sin(radians_angle) / horizontal_stretch
  c = old_center_x - new_center_x * a - new_center_y * b
  d = -math.sin(radians_angle) / vertical_stretch
  e = math.cos(radians_angle) / vertical_stretch
  f = old_center_y - new_center_x * d - new_center_y * e
  image = image.transform(image.size, Image.AFFINE, (a, b, c, d, e, f))

  result = numpy.asarray(image, dtype=theano.config.floatX) / COLOR_BOUNDARY

  if vertical_shift > 0:
    result = numpy.concatenate(
         (numpy.zeros((abs(vertical_shift), 28), dtype=theano.config.floatX),
          result[:-abs(vertical_shift), :]),
                       axis=0)

  if vertical_shift < 0:
    result = numpy.concatenate(
         (result[abs(vertical_shift):, :],
          numpy.zeros((abs(vertical_shift), 28), dtype=theano.config.floatX)),
                       axis=0)

  if horizontal_shift > 0:
    result = numpy.concatenate(
         (numpy.zeros((28, abs(horizontal_shift)), dtype=theano.config.floatX),
          result[:, :-abs(horizontal_shift)]), axis=1)

  if horizontal_shift < 0:
    result = numpy.concatenate(
         (result[:, abs(horizontal_shift):],
          numpy.zeros((28, abs(horizontal_shift)), dtype=theano.config.floatX)), axis=1)

  return result


def get_deformed_data(mnist_data, vertical_shift=0, horizontal_shift=0, rotation_angle=0,
                      vertical_stretch=1., horizontal_stretch=1.):
  return numpy.array([get_deformed_sample(sample.reshape((28, 28)), 
                                          vertical_shift, horizontal_shift,
                                          rotation_angle,
                                          vertical_stretch, horizontal_stretch).reshape(-1)
                      for sample in mnist_data])


# TODO def augmentate(data, labels = NULL): ...
def augmentate(data):
  np_data = data.get_value()
  shifted_samples = []
  for np_image in np_data:
    image = Image.fromarray(numpy.uint8(np_image.reshape(28, 28)
                                                         * COLOR_BOUNDARY))
    image = image.rotate(15)
    new_sample = (numpy.asarray(image,
                  dtype=theano.config.floatX) / COLOR_BOUNDARY).reshape(-1)
    shifted_samples.append(new_sample)
  for np_image in np_data:
    image = Image.fromarray(numpy.uint8(np_image.reshape(28, 28)
                                                         * COLOR_BOUNDARY))
    image = image.rotate(-15)
    new_sample = (numpy.asarray(image,
                  dtype=theano.config.floatX) / COLOR_BOUNDARY).reshape(-1)
    shifted_samples.append(new_sample)
  shifted_data = numpy.array(shifted_samples)
  result = theano.shared(numpy.concatenate((np_data, shifted_data)))
  return result

def symbolic_shift(data_batch, vertical_shift=0, horizontal_shift=0):
  pass
